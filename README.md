# Automatic customization

The main customization package is `adn.sty`. This package loads the different logos as needed, as well as the customization for the current class.

## Automatic class customization

The `adn.sty` package automatically loads a package that matches the name of the current class. For example, if the current class is `foo.cls`, the `adn.sty` package will search for a `adnfoo.sty` package. The main idea is to prepare the customizations needed and often used in this package.

This repository provides customization for 
- Articles (`adnarticle.sty`)
- Reports (`adnreport.sty`)
- Exams (`adnexam.sty`)

# Logos

The documents in this repository allow the use of institutional logos, which are defined within `adn.sty`. The logos are loaded from the `logos` folder. These files should have a `.logo` extension. The name of the logo file will be used as string when loading it within the different source files (see [How to use logos](#how-to-use-logos)).

There are some logos defined (that I often used). However, more can be added just by placing them in the `logo` folder, and following the logo convention (see [New logos](#new-logos)).


## How to use logos

### Setup

The macro that sets the current logo in a given document is `\setlogo{name}` where `name` is the name of the file that contains the logo (that is, `name.logo`). 

The default logo that will be used by `adn.sty` is set in the file `logo.default`. You can modify the macro inside and update the name in the macro. For example

```latex
%% Contents of logo.default
% Set default logo for UNICAMP
\setlogo{UNICAMP}
```

### Advance use

To use the logo and place it manually in your source code, the easiest way is to use the `\getlogo` macro. It expands into the `tikzpicture` definition of the current set logo.

Another way is to use the `\@logoholder[<color>]` which provides a direct expansion into the logo macro, and allows to change the color of it.

## New logos

Current logos use [`tikz`](https://www.ctan.org/pkg/pgf?lang=en) to typeset the logos natively in the documents.

To create a new logo, first, we need the `tikz` code that renders the logo image. In the current logos, the SVG (or PDF) of the logo was transformed by using [Inkscape](https://inkscape.org/en/) and its extension [`inkscape2tikz`](http://code.google.com/p/inkscape2tikz/).

Suppose you have a logo from `XYZ` and want to use the same acronym to load the code (that is, we want to call `\setlogo{XYZ}` in our documents). Then, we need to create a file named `XYZ.logo` with the following contents

```latex
% Logo XYZ eXample of YooZee

\providecommand{\@logoXYZ}{% this should be the acronym, but the \@logo is needed
% insert your logo code, e.g., the obtained redefinition by inkscape2tikz
\begin{tikzpicture}
% omitted code
\end{tikzpicture}
}
```

It is important to note that the file must be available within the `logos`folder.

## Logo macros

Setup
- `\setlogo`
- `\setlogosz`

Getters
- `\getlogo`
- `\@logoholder`


## Colors

Additionally, there are two colors `redadn` y `grayadn` that hold standard colors. This allows to create `tikz` logos and other figures or shapes using the same names, and keep an institutional look and feel.


